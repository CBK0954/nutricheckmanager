package com.lsw.nutricheckmanager.exception;

public class CEatDateFutureException extends RuntimeException {
    public CEatDateFutureException(String msg, Throwable t) {
        super(msg, t);
    }

    public CEatDateFutureException(String msg) {
        super(msg);
    }

    public CEatDateFutureException() {
        super();
    }
}
