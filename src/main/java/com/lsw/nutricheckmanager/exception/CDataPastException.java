package com.lsw.nutricheckmanager.exception;

public class CDataPastException  extends RuntimeException {
    public CDataPastException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDataPastException(String msg) {
        super(msg);
    }

    public CDataPastException() {
        super();
    }
}

