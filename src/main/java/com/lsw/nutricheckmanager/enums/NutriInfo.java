package com.lsw.nutricheckmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor // 값 하나에 아래 '1' 이란 정보들을 한 번에 담아둬야하기 때문에 사용
public enum NutriInfo {

    LUTEIN("루테인", "안구 보호, 백내장 예방", "과량 복용 시 폐암 유발")
    , IRON("철분", "혈액생성", "오심, 구토, 설사, 속쓰림")
    , MINERALS("미네랄", "신체성장 및 기능", "피로, 식욕감퇴, 근육톻, 변비")
    , OMEGA3("오메가3", "심혈관 개선", "알러지 유발")
    , PROPOLIS("프로폴리스", "뼈와 치아 형성", "알러지 유발")
    , PROBIOTICS("프로바이오틱스", "유산균, 장 건강", "설사, 복통, 구토")
    , CALCIUM("칼슘", "관절염 예방", "위장 장애")
    ;

    private final String name; // 1
    private final String effect; // 1
    private final String sideEffect; // 1

}
