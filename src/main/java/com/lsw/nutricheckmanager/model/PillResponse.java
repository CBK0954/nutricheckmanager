package com.lsw.nutricheckmanager.model;

import com.lsw.nutricheckmanager.entity.Pill;
import com.lsw.nutricheckmanager.enums.NutriInfo;
import com.lsw.nutricheckmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PillResponse {

    private Long id;
    private String pillName;
    private String pillCompany;
    private Integer price;
    private NutriInfo nutriInfo;
    private LocalDate startDate;
    private LocalDate expiationDate;
    private Integer totalQuantity;
    private Boolean isDaily;
    private Integer oneTimeQuantity;
    private Integer remainingQuantity;
    private Boolean isEat;

    private PillResponse(PillResponseBuilder builder) {
        this. id = builder.id;
        this. pillName = builder.pillName;
        this. pillCompany = builder.pillCompany;
        this. price = builder.price;
        this. nutriInfo = builder.nutriInfo;
        this. startDate = builder.startDate;
        this. expiationDate = builder.expiationDate;
        this. totalQuantity = builder.totalQuantity;
        this. isDaily = builder.isDaily;
        this. oneTimeQuantity = builder.oneTimeQuantity;
        this. remainingQuantity = builder.remainingQuantity;
        this. isEat = builder.isEat;

    }
    public static class PillResponseBuilder implements CommonModelBuilder<PillResponse> {

        private final Long id;
        private final String pillName;
        private final String pillCompany;
        private final Integer price;
        private final NutriInfo nutriInfo;
        private final LocalDate startDate;
        private final LocalDate expiationDate;
        private final Integer totalQuantity;
        private final Boolean isDaily;
        private final Integer  oneTimeQuantity;
        private final Integer remainingQuantity;
        private final Boolean isEat;

        public PillResponseBuilder(Pill pill) {
            this.id = pill.getId();
            this.pillName = pill.getPillName();
            this.pillCompany = pill.getPillCompany();
            this.price = pill.getPrice();
            this.nutriInfo = pill.getNutriInfo();
            this.startDate = pill.getStartDate();
            this.expiationDate = pill.getExpiationDate();
            this.totalQuantity = pill.getTotalQuantity();
            this.isDaily = pill.getIsDaily();
            this.oneTimeQuantity = pill.getOneTimeQuantity();
            this.remainingQuantity = pill.getRemainingQuantity();
            this.isEat = pill.getIsEat();
        }
        @Override
        public PillResponse build() {
            return new PillResponse(this);
        }
    }


}
