package com.lsw.nutricheckmanager.model;

import com.lsw.nutricheckmanager.entity.Pill;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class HistoryRequest {

    @ApiModelProperty(value = "복용 일자 (yyyy-mm-dd)", required = true)
    @NotNull
    private  LocalDate eatDate;

    // localtime으로 받으면 초까지 적어야하는 번거로움이 생기므로 아래와 같이 시, 분을 나눠 받는다.
    @ApiModelProperty(value = "복용 시간/시 (0~23)", required = true)
    @NotNull
    @Min(value = 0) // 시간의 시작은 0
    @Max(value = 23) // 24시는 표시되지 않음
    private  Integer eatTimeHour;

    @ApiModelProperty(value = "복용 시간/분 (0~59)", required = true)
    @NotNull
    @Min(value = 0)
    @Max(value = 59)
    private  Integer eatTimeMinute;

    @ApiModelProperty(value = "메모 (100자 이내)")
    @Length(max =100)
    private  String memo;
    
}
