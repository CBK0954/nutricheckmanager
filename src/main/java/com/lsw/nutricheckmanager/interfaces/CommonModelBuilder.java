package com.lsw.nutricheckmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
