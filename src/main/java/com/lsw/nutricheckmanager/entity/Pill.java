package com.lsw.nutricheckmanager.entity;

import com.lsw.nutricheckmanager.enums.NutriInfo;
import com.lsw.nutricheckmanager.interfaces.CommonModelBuilder;
import com.lsw.nutricheckmanager.model.PillRequest;
import com.lsw.nutricheckmanager.model.PillUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Pill {

    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "영양제 이름")
    @Column(nullable = false,length = 35)
    private String pillName;

    @ApiModelProperty(notes = "영양제 제조사")
    @Column(nullable = false, length = 20)
    private String pillCompany;

    @ApiModelProperty(notes = "영양제 가격")
    @Column(nullable = false)
    private Integer price;

    @ApiModelProperty(notes = "영양 정보")
    @Column(nullable = false,length = 20)
    @Enumerated(value = EnumType.STRING)
    private NutriInfo nutriInfo;

    @ApiModelProperty(notes = "영양제 복용시작일자")
    @Column(nullable = false)
    private LocalDate startDate;

    @ApiModelProperty(notes = "영양제 유통기한")
    @Column(nullable = false)
    private LocalDate expiationDate;

    @ApiModelProperty(notes = "영양제 총 수량(1회 기준)")
    @Column(nullable = false)
    private Integer totalQuantity; // 총 갯수

    @ApiModelProperty(notes = "섭취 주기 여부")
    @Column(nullable = false)
    private Boolean isDaily; // 일간 : 주간

    @ApiModelProperty(notes = "1회 복용 횟수")
    @Column(nullable = false)
    private Integer  oneTimeQuantity; // 1회 복용 횟수

    @ApiModelProperty(notes = "잔여 수량")
    @Column(nullable = false)
    private Integer remainingQuantity; // 잔여 갯수

    @ApiModelProperty(notes = "현재 복용 여부")
    @Column(nullable = false)
    private Boolean isEat; // 현재 복용 중 : 현재 복용 X

    //먹은거 저장
    // 잔여갯수에서 1회당 복용 수 저장하는 것
    public void putRemainCount(){
        this.remainingQuantity = this.remainingQuantity - this.oneTimeQuantity;

    }

    public void putPillInfo(PillUpdateRequest request){
        this.isDaily = request.getIsDaily();
        this.oneTimeQuantity = request.getOneTimeQuantity();

    }

    private Pill(PillBuilder builder) {
        this.pillName = builder.pillName;
        this.pillCompany = builder.pillCompany;
        this.price = builder.price;
        this.nutriInfo = builder.nutriInfo;
        this.startDate = builder.startDate;
        this.expiationDate = builder.expiationDate;
        this.totalQuantity = builder.totalQuantity;
        this.isDaily = builder.isDaily;
        this.oneTimeQuantity = builder.oneTimeQuantity;
        this.remainingQuantity = builder.remainingQuantity;
        this.isEat = builder.isEat;

    }

    public static class PillBuilder implements CommonModelBuilder<Pill> {
        private final String pillName;
        private final String pillCompany;
        private final Integer price;
        private final NutriInfo nutriInfo;
        private final LocalDate startDate;
        private final LocalDate expiationDate;
        private final Integer totalQuantity;
        private final Boolean isDaily;
        private final Integer oneTimeQuantity;
        private final Integer remainingQuantity;
        private final Boolean isEat;


        public PillBuilder(PillRequest request) {
            this.pillName = request.getPillName();
            this.pillCompany = request.getPillCompany();
            this.price = request.getPrice();
            this.nutriInfo = request.getNutriInfo();
            this.startDate = request.getStartDate();
            this.expiationDate = request.getExpiationDate();
            this.totalQuantity = request.getTotalQuantity();
            this.isDaily = request.getIsDaily();
            this.oneTimeQuantity = request.getOneTimeQuantity();
            this.remainingQuantity = request.getTotalQuantity();
            this.isEat = true;
        }

        @Override
        public Pill build() {
            return new Pill(this);
        }
    }





}
