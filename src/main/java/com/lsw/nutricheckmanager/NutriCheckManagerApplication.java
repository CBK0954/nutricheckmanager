package com.lsw.nutricheckmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NutriCheckManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NutriCheckManagerApplication.class, args);
	}

}
